<?hh //strict
namespace nuclio\plugin\session\jwt
{
	use nuclio\
	{
		core\plugin\Plugin,
		core\ClassManager,
		plugin\http\request\Request,
		plugin\session\CommonSessionInterface
	};
	use Jose\
	{
		Factory\JWKFactory,
		Factory\JWSFactory,
		Loader,
		Object\JWK
	};
	
	use nuclio\plugin\session\jwt\model\JWTSession;
	
	<<
		provides('session::jwt'),
		singleton
	>>
	class JWT extends Plugin implements CommonSessionInterface
	{
		private string $JWT;
		private JWTSession $sessionRecord;
		private Map<array_key,mixed> $data=Map{};
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):JWT
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public static function createToken(Map<string,mixed> $config, string $deviceId):string
		{
			$JWTConfig		=new Map($config->get('jwt'));
			$expireTimestamp=strtotime('now +'.$JWTConfig->get('expirationTime'));
			$key			=JWKFactory::createFromKeyFile
			(
				NUC_TMP_DIR.'nuclio',
				null,
				[
					'kid'=>'My Private RSA key',
					'alg'=>'RS256',
					'use'=>'sig'
				]
			);
			return JWSFactory::createJWSToCompactJSON
			(
				[
					'iss'		=>$JWTConfig->get('issuer'),
					'aud'		=>$JWTConfig->get('audience'),
					'sub'		=>$JWTConfig->get('subject'),
					'exp'		=>$expireTimestamp,
					'iat'		=>time(),
					'nbf'		=>time(),
					'deviceId'	=>$deviceId,
					'salt'		=>hash('sha256',$config->get('crypt.salt'))
				],
				$key,
				['alg'=>'RS256']
			);
		}

		
		public function __construct(Map<string,mixed> $config,?string $JWT=null,?string $deviceId=null)
		{
			parent::__construct();
			
			if (is_null($JWT))
			{
				if (is_null($deviceId))
				{
					$request	=Request::getInstance();
					$deviceId	=$request->getServer('REMOTE_ADDR');
				}
				$JWT=self::createToken($config,$deviceId);
			}
			$this->start($JWT);
		}
		
		public function start(string $JWT):void
		{
			$sessionRecord=JWTSession::findOne(Map{'token'=>$JWT});
			if ($sessionRecord instanceof JWTSession)
			{
				$this->data=new Map(json_decode($sessionRecord->getData(),true));
			}
			else
			{
				$sessionRecord=JWTSession::create
				(
					Map
					{
						'token'			=>$JWT,
						'data'			=>json_encode([]),
						'lastUpdated'	=>date('Y-m-d H:i:s',time()),
						'expires'		=>date('Y-m-d H:i:s',time()+86400*14),
						'status'		=>1
					}
				);
				$sessionRecord->save();
			}
			if ($sessionRecord instanceof JWTSession)
			{
				$this->JWT=$JWT;
				$this->sessionRecord=$sessionRecord;
			}
			else
			{
				//TODO: Exception.
			}
		}
		
		public function migrate(string $newToken):this
		{
			$this->sessionRecord->set('status',0);
			$this->sessionRecord->save();
			$sessionRecord=JWTSession::create
			(
				Map
				{
					'id'			=>$newToken,
					'data'			=>json_encode($this->data->toArray()),
					'lastUpdated'	=>date('Y-m-d H:i:s',time()),
					'expires'		=>date('Y-m-d H:i:s',time()+86400*14),
					'status'		=>1
				}
			);
			$sessionRecord->save();
			if ($sessionRecord instanceof JWTSession)
			{
				$this->JWT=$newToken;
				$this->sessionRecord=$sessionRecord;
			}
			else
			{
				//TODO: Exception.
			}
			return $this;
		}
		
		public function getData():Map<array_key,mixed>
		{
			return $this->data;
		}
		
		public function get(string $key):mixed
		{
			return $this->data->get($key);
		}
		
		public function set(string $key,mixed $value):this
		{
			$this->data->set($key,$value);
			$this->writeSession();
			return $this;
		}
		
		public function delete(string $key):this
		{
			$this->data->remove($key);
			$this->writeSession();
			return $this;
		}
		
		private function writeSession():this
		{
			$this->sessionRecord->setData(json_encode($this->data->toArray()));
			$this->sessionRecord->save();
			return $this;
		}
	}
}
