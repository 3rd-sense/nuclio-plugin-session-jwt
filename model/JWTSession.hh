<?hh //strict
namespace nuclio\plugin\session\jwt\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection JWTSession
	 *
	 * JWTSession data model
	 *
	 * Represent JWTSession collection in a database.
	 */
	class JWTSession extends Model
	{
		/**
		 * @Id
		 *
		 * @var mixed $id
		 *
		 * @access public
		 */
		public mixed $id=null;
		
		/**
		 * @Type text
		 *
		 * @var mixed $token JWTSession Token ID
		 *
		 * @access public
		 */
		public string $token=null;
		
		/**
		 * @Type text
		 *
		 * @var	string|null $title
		 *
		 * @access public
		 */
		public ?string $data=null;
		
		/**
		 * @Type timestamp
		 *
		 * @var	string|null $lastUpdated
		 *
		 * @access public
		 */
		public ?string $lastUpdated=null;
		
		/**
		 * @Type timestamp
		 *
		 * @var	string|null $expires
		 *
		 * @access public
		 */
		public ?string $expires=null;
		
		/**
		 * @Type int
		 * @Length 1
		 *
		 * @var integer|null $status.
		 *
		 * @access public
		 */
		public ?int $status=null;
	}
}
